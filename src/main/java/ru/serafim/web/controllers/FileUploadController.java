package ru.serafim.web.controllers;

// Created by IntelliJ IDEA.
// User: valit
// Date: 13.11.2021
// Time: 10:20
// Project name: serafim
// To change this template use File | Settings | File Templates.

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.serafim.web.dto.FileMetaDataDto;
import ru.serafim.web.services.AccountService;
import ru.serafim.web.services.FileUploadService;

@RequiredArgsConstructor
@Controller
@RequestMapping("/fileUpload")
public class FileUploadController {

    private final FileUploadService fileUploadService;

    private final AccountService accountService;

    @GetMapping
    public String getMapping() {
        return "/fileUpload";
    }

    @PostMapping("/profile/{place_id}")
    public String fileUpload(@RequestParam("file") MultipartFile file,
                             @RequestParam("description") String description,
                             @PathVariable("place_id") Long id,
                             Model model) {
        if(accountService.getAccountById(id).isPresent()) {
            String upload = fileUploadService.upload(file, description, id);
            FileMetaDataDto fileMetaDataDto = fileUploadService.getFileById(
                    upload.substring(upload.indexOf("_") + 1)
            );
            model.addAttribute("file", upload);
            model.addAttribute("Message", upload);
            return "/loadedFile";
        }
        model.addAttribute("Message", "File loaded unsuccessfully, wrong id");
        return "/chillPlace";
    }

    // TODO Insert to file upload construction ChillPlace id
}