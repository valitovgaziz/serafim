package ru.serafim.web.services;

import ru.serafim.web.dto.SignUpForm;

public interface SignUpService {
    void signUp(SignUpForm form);
}
