# Serafim

Resource for searching, filter and fetch the rest place info.

## Это ресурс для поиска мест отдыха.

Ресурс для поиска, фильтрации и получения информации о местах отдыха.

## Состояние проекта.

Реализованы авторизация, аутентификация, регистрация, кое какой фрон на FTLH. Загрузка и регистрация мест отдыха.

## На 12.01.2022.

Решил переделать проект на REST API с фронтом на js.

